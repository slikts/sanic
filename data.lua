local ani = data.raw.character.character.animations
local running = ani[1].running.layers[1]
local shadow = ani[1].running.layers[3]
local layers = { running, shadow }
local speed = 0.2

running.animation_speed = speed
running.hr_version.animation_speed = speed

running.filename = "__Sanic__/graphics/level1_running.png"
running.hr_version.filename = "__Sanic__/graphics/hr-level1_running.png"

shadow.stripes[1].filename = "__Sanic__/graphics/level1_running_shadow-1.png"
shadow.stripes[2].filename = "__Sanic__/graphics/level1_running_shadow-2.png"
shadow.hr_version.stripes[1].filename = "__Sanic__/graphics/hr-level1_running_shadow-1.png"
shadow.hr_version.stripes[2].filename = "__Sanic__/graphics/hr-level1_running_shadow-2.png"

for i = 1, #ani do
    ani[i].running.layers = layers
end
